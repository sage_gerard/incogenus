The **Quolnol** (singular: Quo) are an industrious, yet vulnerable race of squishy scientists near the bottom of the food chain. They use their vast engineering knowledge to ward off predators and disease using defensive technology. They live on a diet of liquids, herbs and soft plants.

## Character
In many ways the Quolnol are like the geeky kid who sits by himself in the cafeteria. He is loaded with potential, but allergies and bullies make it hard for him to get things done. Both the Quolnol and the nerdy kid need a specific environment to function at their best. What makes the Quolnol different from the child is their highly-social nature and ability to quickly adapt to new information. If you hurt one Quolnol, you hurt them all, and you'll probably find yourself on the other side of a force-field in a few seconds.

## Appearance
The Quolnol are a seven-legged freak show made of florescent blue gelatin and charcoal gray alligator scales. It's most striking feature is a neck boasting vocal cords resembling a rib cage. A Quo speaks in whistles, clicks and squeaks in normal conversation. When threatened, they can produce an unbearable shriek that will almost certainly either catch another Quo's attention or scare away the threat.

Each Quo is about the size of a golf card when standing on it's hind five legs. One leg protrudes directly behind its abdomen and the other four are arranged in a manner more familiar to quadrapeds. Every leg terminates in grabbers used to manipulate objects except the rear three legs, which are more muscular and used for balance.

A Quolnol has seven eyes pocking down its slender visage in delta formation. Six eyes move independently and detects motion quickly, but the single large eye directly on the front of its face can look only forwards, and had remarkable depth perception.

Between the eyes, vocal cords, skin and legs a Quolnol looks like nothing other than a Quolnol. One could describe it as a glowing blue Jell-O dinosaur bug with a goddamn meat harp for a mouth, but most might instead opt for one prolonged scream. Description often cannot do justice to the actual experience for an observer.


## Culture
The Quolnol's home planet is full of nasty things that want them dead, so a young Quolnol is immediately under a lot of pressure to at the very least go to med school. Quolnol hives are fully-enclosed cities built with a well-considered infrastructure. Like a bee, your job as a Quo is to keep the hive alive, but unlike the bee you have the self-awareness to hate your job. You may take breaks and go have a night out on the town, but if you fail to patch up that hole in the hive shell, then horrible things will come through and eat your parents.

A Quo's brain is its best asset. With proper training he doesn't need to spend much effort writing up specifications and figuring out how to construct a building. He does not have a great deal of physical strength so most projects require a team to complete. 

Quolnol colonies are so survival-focused that even the most tedious jobs are considered worthy of respect in times of peace. However, criminals and slackers are treated like vermin. The hive figures that if you act like one of the jerks outside the city walls then you can jolly well live with *them*. Even the most brilliant Quos often need teams to provide the means to survive, so exiling a single Quo is effectively a death sentence. This is why the justice system tends to mandate single file lines to minimize risk of accidentally forming outside revolutionary groups.

Despite having the means to build great cities and make scientific breakthroughs, the Quolnol desperately want to be lazy. Although if given the oppurtunity to relax and never worry again, the Quolnol would probably idle and reflect in horror on the face they never actually got a chance to *live* before. Most Quolnol defensive technology includes low-maintenance force fields and sturdy enclosures because building weapons and shooting things is just more work. The Quolnol don't particularly care about having predators sleeping on the roof so long as the fangs and the venom and the hunger stay on that side of the enclosure. The Quolnol are not pacifist, but they consider combat an option to any threat that would tear down the walls of the hive or make obtaining food difficult. Civil war almost never occurs because each Quolnol hive is so xenophobic that everything outside the hive is hardly worth the trouble, and everyone inside is a friend that keeps the massive roof over everyone's head.


## What happens if you meet a Quo
If you encounter a Quo and resist the urge to vomit or run away screaming, the Quo might just do that for you. The Quolnol feel their being born on a planet full of death traps is part of a sick joke where the punchline involves mass PTSD and trust issues. A Quo has absolutely no reason to see you as a friend, and it isn't particularly enthused about the prospect. If a Quo stands in your vicinity without opening it's harp to tear nearby eardrums, it either has not noticed you or you are one of the rare few to get by the xenophobia and actually appear useful.
