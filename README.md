Incogenus is a science fiction story written in LaTeX using a process inspired by both [Git Flow](https://github.com/nvie/gitflow) and [The Snowflake Method](http://www.advancedfictionwriting.com/articles/snowflake-method/).


## File system

The LaTeX project compiles to PDF in `dist/` using files in `story/`. All files are named such that one can infer their purpose, but you can look at other `README.md` files for more specific information about the directories in which they reside.

`subjects/` contains art assets and clear text definitions for characters, settings, conflicts and plot decisions.






## Version control

The branches behave similarly to their Git Flow counterparts in parentheticals. They differ in name and extend on the expected behavior of the author defined by Git Flow.

- The `master` branch holds a project that compiles to PDF and tells a complete story (These conditions are strictly enforced).
- An `elaboration` (`develop`) branch adds detail to the story in `master` or adjusts concepts in `subjects/` to fit the story. Alternatively, `elaboration` may modify the LaTeX source for technical reasons.
- A `subject` (`feature`) branch introduces and develops at least one unique, self-contained concept in `subjects/` later used to elaborate the story. 
- A `review` (`release`) branch holds a complete story in review. If the story is of high enough quality it may merge to `master`.
- A `reconciliation` (`hotfix`) branch is meant to fix glaring logical errors or gaping plotholes that require immediate attention. Take care with the project to minimize the chance that you need to create this branch.
- A `seed` (`support`) branch addresses problems in the story for a future review.






## Project lifecycle

While `master` must hold a complete story, that does not mean the story is ready for publication. Case in point: The first commit to `master` will tell the entire novel's story in too few words to warrant a good novel.

This project will face a "soft end" on a commit to `master` representing a finished novel to package and sell. Since there is no need to measure progression or compatibility, there are no version numbers for this project and release tags are arbitrary. The novel is either finished or not, and a commit to `master` will indicate if this is the case.

After novel completion `subjects/` stops serving as a tool for the author and starts serving as "behind the scenes" content for readers, which may be seperately packaged and sold.

It is possible to reopen a finished novel for modification, but only if there is no change to the semantics of the story. With a finished novel you may only fix errors and prepare the story for a new medium. You may also introduce new content in `subjects/` so long as there is no indication of change in the story.







## Elaboration process

The `elaboration` branch may either expand on the story or perform technical tasks using LaTeX. This section applies to the former scenario.

When writing the story, use footnotes containing at least one question or instruction to identify oppurtunities to build on the story.

For example:

```
Bob jumped over Alice. \footnote{Why did Bob jump over Alice?}
```

You should write content that addresses a footnote either in the vicinity of where the footnote appeared, or in a location that better establishes context. When you remove the basis on which the footnote stands, remove the footnote.

When you commit to `elaboration` you may leave behind as many footnotes as you wish, but should always remove at least one. **By the time you start a `review` branch there should not exist any footnotes meant to assist writing.**






## Review process

1. Elaborate on all relevant footnotes.
2. `git flow release start ...`
3. Compile PDF and release to trusted readers for feedback.
4. From the feedback, insert a list of footnotes where applicable according to your own best judgement. Solve problems, but do not try to make everyone happy. You know what's best for your novel.
5. Address all footnotes.
6. Repeat steps 3-6 until there exist no footnotes.
7. `git flow release finish`






## Guidelines and rules

- Do not adjust the `story/` to the `subjects/`, adjust the `subjects/` to the `story/`.
- Do not modify the `story/` when on a `subject` branch. Only modify `subjects/`. When you do not know what the story needs you should not touch it.
- Do not modify `subjects/` when on a `elaboration` branch. Only modify the `story/`. When you are elaborating you should already know enough about what the story needs to continue writing.
- Do not finish a `subject` branch until it met its intended purpose. If the idea does not go anywhere, delete the branch. Better yet, if you are totally unaware of what to even write about a subject, do not touch the project at all. Sketch ideas on pen and paper before returning to work.
- Be careful with the story and keep an eye out for plotholes and alternative options for the characters. Playing fast and loose means `reconciliation` and `seed` branches, which won't be fun.
- A reviewer should not have to be a developer. Allow more than one person to inspect a `review` branch by reading the PDF output of the project.